extends Node2D

var chunkScene = preload("res://scn/chunk.scn")
var big_cactus = preload("res://scn/big_cactus.scn")
var small_cactus = preload("res://scn/small_cactus.scn")
var bird = preload("res://scn/bird_object.scn")
@onready var localDino = $DinoLocal/DinoBody
@onready var chunks = $Chunks

var lastCreatedAt = 0
func _create_cactuses(chunkId:int, chunk:Node2D):
	if chunkId == 0: return
	var bodyVelX = 500
	var dinoBody = SharedScript.LocalDino.get_node_or_null("DinoBody")
	if dinoBody != null: bodyVelX = dinoBody.velocity.x * 5
	var currentlyAt = lastCreatedAt
	while currentlyAt < chunk.global_position.x+576:
		var spawnOffset = randf_range(2, 45)
		var spawnPosX = currentlyAt+bodyVelX+spawnOffset
		if randi_range(0,50) > 1:
			var what2Spawn = big_cactus
			if randi_range(0,10) < 5: what2Spawn = small_cactus
			var newCactus = what2Spawn.instantiate()
			chunk.get_node("Cactuses").add_child(newCactus)
			newCactus.global_position = Vector2(spawnPosX, 530)
			currentlyAt = spawnPosX
		else:
			var newBird = bird.instantiate()
			add_child(newBird)
			if randi_range(0,1) == 0:
				newBird.global_position = Vector2(spawnPosX, 465)
			else:
				newBird.global_position = Vector2(spawnPosX, 540)
	lastCreatedAt = currentlyAt

func _create_chunk(chunkId:int):
	var newChunk = chunkScene.instantiate()
	newChunk.position.x = chunkId*1152
	newChunk.name = str(chunkId)
	_create_cactuses(chunkId, newChunk)
	chunks.add_child(newChunk)

var lastCheck = -1
func _ready() -> void:
	seed(Time.get_unix_time_from_system())
	SharedScript.GameWorld = self
	SharedScript.RemoteContainer = $Remotes

func _physics_process(_delta: float) -> void:
	var dinoPos = localDino.global_position
	if dinoPos.x == 0: dinoPos.x = .1
	var xChunkId = floor(dinoPos.x / 1152)
	if xChunkId == lastCheck: return
	var lastChunk = chunks.get_node_or_null(str(xChunkId-1))
	if lastChunk: lastChunk.queue_free()
	var currentChunk = chunks.get_node_or_null(str(xChunkId))
	if currentChunk == null: _create_chunk(xChunkId)
	var nextChunk = chunks.get_node_or_null(str(xChunkId+1))
	if nextChunk == null: _create_chunk(xChunkId+1)
	var nextChunk2 = chunks.get_node_or_null(str(xChunkId+2))
	if nextChunk2 == null: _create_chunk(xChunkId+2)
	lastCheck = xChunkId
