extends Area2D

@onready var listOfTexturePaths = [
	["res://png/cactus_big_01.png", [$coll_01]],
	["res://png/cactus_big_02.png", [$coll_02]],
	["res://png/cactus_big_03.png", [$coll_03]],
	["res://png/cactus_big_04.png", [$coll_04]],
	["res://png/cactus_big_05.png", [$coll_05]],
	["res://png/cactus_big_06.png", [$coll_06, $coll_06_tiny]],
]

func _ready() -> void:
	seed(SharedScript.RemoteSeed+get_parent().get_parent().name.to_int())
	var textureData = listOfTexturePaths[randi_range(0, listOfTexturePaths.size()-1)]
	var texture = load(textureData[0])
	$CactusSprite.texture = texture
	for coll in textureData[1]:
		coll.disabled = false
	for child in get_children():
		if child is CollisionPolygon2D:
			if child.disabled: child.queue_free()

func _body_entered(body: Node2D) -> void:
	if body == SharedScript.LocalDino:
		body.emit_signal("cactus_touched", self)
		queue_free()
