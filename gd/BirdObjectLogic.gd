extends Area2D


func _physics_process(delta: float) -> void:
	global_position.x -= 250 * delta

func _on_body_entered(body: Node2D) -> void:
	if body == SharedScript.LocalDino:
		body.emit_signal("bird_touched", self)
