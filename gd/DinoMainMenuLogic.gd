extends Control

var gameScene = preload("res://scn/generated_map.tscn")
var savedServersList = [["127.0.0.1", 18176, true],["45.59.70.89", 18176, true]]

func _ready() -> void:
	SharedScript.CurrentMenu = self

func _on_singleplayer_pressed() -> void:
	await get_tree().process_frame
	get_tree().change_scene_to_packed(gameScene)

func _on_multiplayer_pressed() -> void:
	#ConnectionManager.cm_connect("127.0.0.1", 18176)
	$Main.visible = false
	for server in $Servers/ServerList/Vertical.get_children():
		if server.visible: server.queue_free()
	$Servers.visible = true
	for server in savedServersList:
		WebSocketManager.ws_create_client(server[0],server[1]+1000, server)
func _on_settings_pressed() -> void:
	pass # Replace with function body.

func _on_quit_pressed() -> void:
	get_tree().quit()

func _on_back_pressed_servers() -> void:
	$Servers.visible = false
	$Main.visible = true

func _mouse_hover() -> void:
	$Hover.play()
func _mouse_click() -> void:
	$Click.play()
