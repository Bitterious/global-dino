extends CharacterBody2D

signal remote_jump()
signal remote_sync_position(pos:Vector2)
signal remote_update_health(health:int)
signal remote_update_speed(speed:float)
signal remote_sync_crouch(status:bool)
signal remote_died()

const JUMP_VELOCITY = -600.0
var TARGET_SPEED = 300
var SPEED = 0
var HEALTH = 3
var LERP_LEVEL = .4
var HEARTBEAT = true
var CROUCHING = false

var gravity: int = ProjectSettings.get_setting("physics/2d/default_gravity")
@onready var baseSprite = $DinoSpriteBase
@onready var damageSprite = $DinoSpriteDamage
@onready var jumpSprite = $DinoSpriteJump
@onready var crouchSprite = $DinoSpriteCrouch
@onready var crouchIdleSprite = $DinoSpriteCrouchIdle
@onready var deadSprite = $DinoSpriteDead
@onready var healthSprites = $Health

@onready var pointLabel = $PointLabel

func _enable_sprite(sprite:AnimatedSprite2D) -> void:
	if sprite.visible: return
	baseSprite.visible = false
	jumpSprite.visible = false
	crouchSprite.visible = false
	crouchIdleSprite.visible = false
	damageSprite.visible = false
	sprite.visible = true
	if !sprite.is_playing():
		sprite.play("default")

func _ready() -> void:
	crouchSprite.connect("animation_finished", func():
		if is_on_floor() and !jumpSprite.visible and HEARTBEAT: _enable_sprite(crouchIdleSprite))
	damageSprite.connect("animation_finished", func():
		if HEARTBEAT: _enable_sprite(baseSprite))

func _physics_process(_delta: float) -> void:
	if is_on_floor():
		if !baseSprite.visible:
			if jumpSprite.visible && !crouchSprite.visible:
				_enable_sprite(baseSprite)
	if CROUCHING and is_on_floor() and HEARTBEAT:
		if !crouchIdleSprite.visible:
			_enable_sprite(crouchSprite)
	elif HEARTBEAT:
		if is_on_floor() and !jumpSprite.visible and !damageSprite.visible:
			_enable_sprite(baseSprite)
	pointLabel.text = str(floor(global_position.x*.05))

func _remote_jump() -> void:
	if is_on_floor() and !(crouchSprite.visible||\
	crouchIdleSprite.visible) and HEARTBEAT:
		velocity.y = JUMP_VELOCITY
		_enable_sprite(jumpSprite)

var connectionInterp = .255
func _on_remote_sync_crouch(status: bool) -> void: CROUCHING = status
func _on_remote_died() -> void: HEARTBEAT = false
func _on_remote_sync_position(pos: Vector2) -> void:
	create_tween().tween_property(self, "global_position", pos, connectionInterp)
func _on_remote_update_speed(speed: float, target: float) -> void: SPEED = speed; TARGET_SPEED = target

var fullHP = preload("res://png/dino_health_full.png")
var lowHP = preload("res://png/dino_health_empty.png")
func _on_remote_update_health(healthv: int) -> void:
	HEALTH = healthv
	for health in healthSprites.get_children():
		var intName = health.name.to_int()
		if intName <= HEALTH: health.texture = fullHP
		else: health.texture = lowHP
	LERP_LEVEL -= .05 * (3-HEALTH)
	_enable_sprite(damageSprite)
	if HEALTH <= 0:
		$NameLabel.text = "DINO\n*DEAD*"
		HEARTBEAT = false
		_enable_sprite(deadSprite)
