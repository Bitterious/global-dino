extends CharacterBody2D

signal cactus_touched(cactus:Area2D)
signal bird_touched(bird:Area2D)

var fullHP = preload("res://png/dino_health_full.png")
var lowHP = preload("res://png/dino_health_empty.png")

var nightShader = preload("res://material/invert_colors_night_time.material")

const JUMP_VELOCITY = -600.0
var TARGET_SPEED = 300
var MAX_SPEED = 800
var SPEED = 0
var ACCEL = 1.2
var HEALTH = 3
var LERP_LEVEL = .4
var HEARTBEAT = true
var CROUCHING = false

var gravity: int = ProjectSettings.get_setting("physics/2d/default_gravity")
@onready var pointLabel = $PointLabel
@onready var baseSprite = $DinoSpriteBase
@onready var damageSprite = $DinoSpriteDamage
@onready var jumpSprite = $DinoSpriteJump
@onready var crouchSprite = $DinoSpriteCrouch
@onready var crouchIdleSprite = $DinoSpriteCrouchIdle
@onready var deadSprite = $DinoSpriteDead
@onready var healthSprites = $Health
@onready var crownSprite = $Crown

@onready var jumpSFX = $Jump
@onready var hurtSFX = $Hurt
@onready var cactusHitSFX = $CactusHit/Explode
@onready var boostedSFX = $Booster
@onready var milestoneSFX = $Milestone

@onready var defaultColl = $DinoColl
@onready var crouchColl = $DinoCollCrouch

var camera:Camera2D
var ui:Control

var speedText:Label
var yourPoints:Label
var leadPoints:Label

var animsToInteract = []

func _enable_sprite(sprite:AnimatedSprite2D) -> void:
	if sprite.visible: return
	for anim in animsToInteract:
		if anim == sprite: continue
		anim.visible = false
		anim.stop()
	sprite.visible = true
	if !sprite.is_playing():
		sprite.play("default")

func _ready() -> void:
	SharedScript.LocalDino = self
	camera = get_parent().get_node("DinoCam")
	SharedScript.CurrentCamera = camera
	ui = camera.get_node("DinoUI")
	speedText = ui.get_node("Speed")
	yourPoints = ui.get_node("YourPoints")
	leadPoints = ui.get_node("LeadPoints")
	crouchSprite.connect("animation_finished", func():
		if is_on_floor() and !jumpSprite.visible and HEARTBEAT: _enable_sprite(crouchIdleSprite))
	damageSprite.connect("animation_finished", func():
		if HEARTBEAT: _enable_sprite(baseSprite))
	animsToInteract = [baseSprite, jumpSprite, crouchSprite, 
	crouchIdleSprite, damageSprite]
	SharedScript.emit_signal("GameLoaded")

var lastTickPos = Vector2.ZERO
var lastTickSpd = 0
var lastMileStone = 0
func _physics_process(delta: float) -> void:
	if TARGET_SPEED <= MAX_SPEED:
		TARGET_SPEED += ACCEL*delta
	SPEED = lerpf(SPEED, TARGET_SPEED, LERP_LEVEL*delta)
	if not is_on_floor():
		velocity.y += gravity * delta
	else:
		if !baseSprite.visible:
			if jumpSprite.visible && !crouchSprite.visible:
				_enable_sprite(baseSprite)
	var milestoneCurrent = floor(global_position.x *.0005)
	if lastMileStone < milestoneCurrent:
		milestoneSFX.play()
		lastMileStone = milestoneCurrent
	if Input.is_action_pressed("game_jump") and is_on_floor()\
	and !(crouchSprite.visible||crouchIdleSprite.visible) and HEARTBEAT:
		velocity.y = JUMP_VELOCITY
		jumpSFX.play()
		_enable_sprite(jumpSprite)
		ConnectionManager.api_send_packet(6, [])
	if Input.is_action_pressed("game_crouch") and is_on_floor() and HEARTBEAT:
		if !crouchIdleSprite.visible:
			if !CROUCHING:
				CROUCHING = true
				ConnectionManager.api_send_packet(1, [CROUCHING])
			_enable_sprite(crouchSprite)
			defaultColl.disabled = true
			crouchColl.disabled = false
	elif HEARTBEAT:
		if is_on_floor() and !jumpSprite.visible and !damageSprite.visible:
			_enable_sprite(baseSprite)
			defaultColl.disabled = false
			crouchColl.disabled = true
			if CROUCHING:
				CROUCHING = false
				ConnectionManager.api_send_packet(1, [CROUCHING])
	velocity.x = SPEED
	if !HEARTBEAT: velocity.x = 0
	move_and_slide()
	if global_position != lastTickPos && Engine.get_physics_frames() % 12 == 0:
		ConnectionManager.api_send_packet(2, [global_position])
		lastTickPos = global_position
	var selfPoints:String = str(int(global_position.x*.05))
	pointLabel.text = str(selfPoints)
	for index in range(6-selfPoints.length()):
		selfPoints = "0"+selfPoints
	yourPoints.text = "%point YOU".format([selfPoints], "%point")
	speedText.text = "SPEED\n%speedp/s".format([str(int(velocity.x*.5)*.1)], "%speed")
	var highestX = global_position.x
	var furthestCrown = crownSprite
	for dino in SharedScript.RemoteContainer.get_children():
		var body: CharacterBody2D = dino.get_node("DinoBody")
		var crown: Sprite2D = body.get_node("Crown")
		crown.visible = false
		if body.global_position.x > highestX: 
			highestX = body.global_position.x
			furthestCrown = crown
	var leadsPoints = str(int(highestX*.05))
	crownSprite.visible = false
	furthestCrown.visible = true
	for index in range(6-leadsPoints.length()):
		leadsPoints = "0"+leadsPoints
	leadPoints.text = "LEAD %point".format([leadsPoints], "%point")
	if int(global_position.x*.05)%1000 >= 700:
		pass

func _process(_delta: float) -> void:
	camera.position.x = position.x - 110

func _respawn() -> void:
	HEALTH = 3
	TARGET_SPEED = 300
	LERP_LEVEL = .4
	HEARTBEAT = true

func _damage_player() -> void:
	HEALTH -= 1
	for health in healthSprites.get_children():
		var intName = health.name.to_int()
		if intName <= HEALTH: health.texture = fullHP
		else: health.texture = lowHP
	SPEED = 0
	LERP_LEVEL -= .05
	_enable_sprite(damageSprite)
	hurtSFX.play()
	if HEALTH <= 0:
		$NameLabel.text = "DINO\n*DEAD*"
		HEARTBEAT = false
		_enable_sprite(deadSprite)
	ConnectionManager.api_send_packet(3, [HEALTH])

func _on_cactus_touched(_cactus: Area2D) -> void:
	if !HEARTBEAT: return
	_damage_player()
	$CactusHit.restart()
	cactusHitSFX.play()

func _on_bird_touched(_bird: Area2D) -> void:
	if !HEARTBEAT: return
	_damage_player()
	_bird.queue_free()
	#TODO: ADD EFFECTS
