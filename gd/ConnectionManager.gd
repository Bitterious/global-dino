extends Node

var dinoRemote = preload("res://scn/dino_remote.scn")
var gameScene = preload("res://scn/generated_map.tscn")
var mpApi:MultiplayerAPI = null

var packets = [
	func(args:Array): # join sync players 0
		var peerListToSave = {}
		var serverSeed:int = args[1]
		SharedScript.RemoteSeed = serverSeed
		var serverPeers:Dictionary = args[0]
		for peerid in serverPeers.keys():
			var peer = serverPeers[peerid]
			var newDino = dinoRemote.instantiate()
			newDino.name = "Peer"+str(peerid)
			var body = newDino.get_node("DinoBody")
			SharedScript.RemoteContainer.add_child(newDino)
			body.emit_signal("remote_sync_position", peer[0])
			body.emit_signal("remote_sync_crouch", peer[1])
			body.emit_signal("remote_update_health", peer[2])
			body.emit_signal("remote_update_speed", peer[3], peer[4])
			peerListToSave[peerid] = newDino
		SharedScript.RemoteIdList = peerListToSave
		var localDino = SharedScript.LocalDino
		rpc_id(1, "api_peer_rpc", 5, [
			localDino.global_position,
			localDino.get("CROUCHING"),
			localDino.get("HEALTH"),
			localDino.get("SPEED"), localDino.get("TARGET_SPEED")
		]),
	func(args:Array): # new player joined 1
		var peerId = args[0]
		if peerId == mpApi.get_unique_id(): return
		var newDino = dinoRemote.instantiate()
		newDino.name = "Peer"+str(peerId)
		var body = newDino.get_node("DinoBody")
		SharedScript.RemoteContainer.add_child(newDino)
		body.emit_signal("remote_sync_position", args[1][0])
		body.emit_signal("remote_sync_crouch", args[1][1])
		body.emit_signal("remote_update_health", args[1][2])
		body.emit_signal("remote_update_speed", args[1][3], args[1][4])
		SharedScript.RemoteIdList[peerId] = newDino,
	func(args:Array): # player left free 2
		var peerId = args[0]
		if peerId == mpApi.get_unique_id(): return
		if !SharedScript.RemoteIdList.has(peerId): return
		var dino2Kill = SharedScript.RemoteIdList[peerId]
		dino2Kill.queue_free(),
	func(args:Array): # player jump 3
		var peerId = args[0]
		if peerId == mpApi.get_unique_id(): return
		if !SharedScript.RemoteIdList.has(peerId): return
		var dino2Emit:Node2D = SharedScript.RemoteIdList[peerId]
		dino2Emit.get_node("DinoBody").emit_signal("remote_jump"),
	func(args:Array): # player hurt 4
		var peerId = args[0]
		if peerId == mpApi.get_unique_id(): return
		if !SharedScript.RemoteIdList.has(peerId): return
		var dino2Emit:Node2D = SharedScript.RemoteIdList[peerId]
		dino2Emit.get_node("DinoBody").emit_signal("remote_update_health", args[1]),
	func(args:Array): # player pos sync 5
		var peerId = args[0]
		if peerId == mpApi.get_unique_id(): return
		if !SharedScript.RemoteIdList.has(peerId): return
		var dino2Emit:Node2D = SharedScript.RemoteIdList[peerId]
		dino2Emit.get_node("DinoBody").emit_signal("remote_sync_position", args[1]),
	func(args:Array): # player speed sync 6
		var peerId = args[0]
		if peerId == mpApi.get_unique_id(): return
		if !SharedScript.RemoteIdList.has(peerId): return
		var dino2Emit:Node2D = SharedScript.RemoteIdList[peerId]
		dino2Emit.get_node("DinoBody").emit_signal("remote_update_speed", args[1], args[2]),
	func(args:Array): #player crouch sync 7
		var peerId = args[0]
		if peerId == mpApi.get_unique_id(): return
		if !SharedScript.RemoteIdList.has(peerId): return
		var dino2Emit:Node2D = SharedScript.RemoteIdList[peerId]
		dino2Emit.get_node("DinoBody").emit_signal("remote_sync_crouch", args[1]),
]

var playerUI = null
var pingLabel:Label = null

func cm_connect(ip:String, port:int) -> void:
	print_rich("[color=green]Connecting to the server '", ip+":"+str(port), "'.[/color]")
	var newMP = MultiplayerAPI.create_default_interface()
	mpApi = newMP
	newMP.connect("connected_to_server", Callable(api_connected).bind(newMP))
	newMP.connect("connection_failed", Callable(api_failed).bind(newMP))
	var newPeer = ENetMultiplayerPeer.new()
	newPeer.create_client(ip, port)
	newMP.multiplayer_peer = newPeer
	get_tree().set_multiplayer(newMP, get_path())

func api_connected(_mpapi:SceneMultiplayer) -> void:
	print_rich("[color=green]Connected server. Loading game...[/color]")
	get_tree().change_scene_to_packed(gameScene)
	print_rich("[color=green]Loaded the scene. Waiting for initialization...[/color]")
	await SharedScript.GameLoaded
	print_rich("[color=green]Loading finished. Starting the game.[/color]")
	playerUI = SharedScript.CurrentCamera.get_node("DinoUI")
	pingLabel = playerUI.get_node("ConnectionPing")
	rpc_id(1, "api_peer_rpc", 0, ["null"])

func api_failed(_mpapi:SceneMultiplayer) -> void:
	print("not yeat")

@rpc("authority", "unreliable_ordered")
func api_peer_rpc(packet:int, args:Array) -> void:
	if packets[packet]: packets[packet].call(args)

func api_send_packet(packet:int, data:Array) -> void:
	if not mpApi: return
	if mpApi.multiplayer_peer.get_connection_status()\
	!= MultiplayerPeer.CONNECTION_CONNECTED: return
	rpc_id(1, "api_peer_rpc", packet, data)

#func _process(delta: float) -> void:
	#if Engine.get_process_frames() % 20 == 0:
		#pingLabel = mpApi

func cm_disconnect() -> void:
	print_rich("[color=red]Disconnecting from the server.[/color]")
	mpApi.multiplayer_peer.close()
	mpApi = null
