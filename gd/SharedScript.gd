extends Node

signal GameLoaded()

var CurrentMenu : Control = null
var CurrentCamera : Camera2D = null
var LocalDino : Node2D = null
var GameWorld: Node2D = null
var RemoteContainer : Node2D = null
var RemoteIdList : Dictionary = {}
var RemoteSeed : int = 0
