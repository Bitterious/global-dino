extends Node

signal finished()

var currentMP:MultiplayerAPI = null
var busy:bool = false
var packets:Array = [
	func(args:Array): # server data 0
		var serverName = args[0]
		var playerCount = args[1]
		var maxPlayers = args[2]
		var serverIp = args[3]
		var serverPort = args[4]
		var removable = args[5]
		var serverBind = SharedScript.CurrentMenu.get_node\
		("Servers/ServerList/Vertical/Template").duplicate()
		serverBind.visible = true
		serverBind.get_node("ServerName").text = serverName
		serverBind.get_node("ServerLimit").text = "| %val/%val |".format([playerCount,maxPlayers], "%val")
		serverBind.get_node("Join").connect("pressed", Callable(ConnectionManager.cm_connect).bind(serverIp, serverPort))
		serverBind.get_node("Remove").disabled = removable
		serverBind.get_node("Remove").connect("pressed", Callable(func(): serverBind.queue_free()))
		SharedScript.CurrentMenu.get_node("Servers/ServerList/Vertical").add_child(serverBind)
		busy = false
		emit_signal("finished")
]

func ws_create_client(ip:String, port:int, bindArgs:Array):
	if busy:
		await finished
	busy = true
	var newMP = MultiplayerAPI.create_default_interface()
	currentMP = newMP
	var newPeer = WebSocketMultiplayerPeer.new()
	newPeer.create_client("ws://"+ip+":"+str(port))
	newMP.multiplayer_peer = newPeer
	newMP.connect("connected_to_server", Callable(api_connection_success).bind(bindArgs))
	newMP.connect("connection_failed", Callable(api_connection_failed))
	get_tree().set_multiplayer(newMP, get_path())

func api_connection_success(bindedArgs:Array):
	rpc_id(1, "api_peer_rpc", 0, bindedArgs)

func api_connection_failed():
	busy = false
	emit_signal("finished")

@rpc("authority")
func api_peer_rpc(packet:int, args:Array = []) -> void:
	if packets[packet]:
		packets[packet].call(args)
